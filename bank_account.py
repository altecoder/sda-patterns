# BankAccount - subject ** state
# channel - Invoker
# Command - Debit/ Credit/ Rollback


class BankAccount(object):
    def __init__(self, balance=0):
        self.balance = balance
        self.previous_balance = balance

    def debit(self, amount):
        self.previous_balance = self.balance
        self.balance = self.balance - amount

    def credit(self, amount):
        self.previous_balance = self.balance
        self.balance = self.balance + amount

    def __str__(self):
        return self.balance


class CommandInterface:
    def __init__(self, account):
        self.account = account
        self.last_balance = account.balance

    def execute(self):
        pass


class DebitCommand(CommandInterface):
    def __init__(self, account, amount):
        super().__init__(account)
        self.amount = amount

    def execute(self):
        self.account.debit(self.amount)

    def reverse(self):
        self.account.credit(self.amount)


class AccountCommandInvoker:
    def __init__(self):
        self.actions = []

    def save_and_execute(self, command):
        command.execute()
        self.actions.append(command)

    def reverse(self):
        if self.actions:
            self.actions.pop().reverse()


if __name__ == "__main__":
    my_account = BankAccount(100)
    print(f"Initial Balance = {my_account.__str__()}")
    client = AccountCommandInvoker()
    debit_com = DebitCommand(my_account, 10)
    client.save_and_execute(debit_com)
    print(f"New Balance = {my_account.__str__()}")
    client.reverse()
    print(f"New Balance after Reverse = {my_account.__str__()}")
